import React from "react";
import { TimerList, Header } from "./components";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Header />
      <TimerList />
    </div>
  );
}

export default App;
