import React, { useEffect, useMemo, useState } from "react";
import {
  Card,
  Button,
  createStyles,
  makeStyles,
} from "@material-ui/core";

import { Timer } from "../types/timer";
import { TIMER_STATUSES } from "../constants";
import { useAppDispatch } from "../store/hooks";
import { addTimer } from "../store/timers/actions";
import start from "../assets/play.svg";
import stop from "../assets/stop.svg";
import check from "../assets/check.svg";

const useStyles = makeStyles(() =>
  createStyles({
    card: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: 'center',
      padding: "0 5px 0 5px",
      width: 380,
      borderRadius: 12,
      backgroundColor: '#acb6c5',
      boxShadow: "0 2px 4px 0 rgba(138, 148, 159, 0.2)",
    },
    timer: {
      display: 'flex',
      alignItems: 'center',
    },
    button: {
      height: 25,
      paddingRight: 35,
      '& disabled': {
        color: '#fad7cd'
      }
    },
    seconds: {
      color: '#e64311',
      display: 'flex',
      paddingRight: 2,
    },
    icon: {
      height: 20,
      width: 20,
      opacity: 1,
      transition: '0.5s linear',
      position: 'absolute',
      left: 60,
    },
    hidden: {
      opacity: 0,
    }
  })
);

interface TimerCardProps {
  timer: Timer;
}

export const TimerCard: React.FunctionComponent<TimerCardProps> = ({
  timer,
}) => {
  const classes = useStyles();
  const [seconds, setSeconds] = useState(timer.seconds);
  const [status, setStatus] = useState(timer.status);
  const dispatch = useAppDispatch();

  useEffect(() => {
    const currentTimer = setInterval(() => {
      if (status === TIMER_STATUSES.IN_PROGRESS) setSeconds(seconds + 1);
    }, 1000);

    return () => {
      clearInterval(currentTimer);
    };
  });

  const onStartClick = () => {
    setStatus(TIMER_STATUSES.IN_PROGRESS);
  };

  const onStopClick = () => {
    setStatus(TIMER_STATUSES.DONE);
    dispatch(addTimer({ ...timer, status, seconds }));
  };

  const buttonData: {
    text: string;
    onClick?: () => void;
  } = useMemo(() => {
    switch (status) {
      case TIMER_STATUSES.DONE:
        return { text: "Done" };
      case TIMER_STATUSES.IN_PROGRESS:
        return { text: "Stop", onClick: onStopClick };
      default:
        return { text: "Start", onClick: onStartClick };
    }
  }, [status]);
  return (
    <Card className={classes.card}>
      <Button
        className={classes.button}
        disabled={status === TIMER_STATUSES.DONE}
        onClick={buttonData.onClick}
      >
        {buttonData.text}
        
        <img className={`${classes.icon} ${status !== TIMER_STATUSES.START &&  classes.hidden}`} src={start}  alt=""/>
        <img className={`${classes.icon} ${status !== TIMER_STATUSES.IN_PROGRESS &&  classes.hidden}`} src={stop}  alt=""/>
        <img className={`${classes.icon} ${status !== TIMER_STATUSES.DONE &&  classes.hidden}`} src={check}  alt=""/>
      </Button>
      <span className={classes.timer}><p className={classes.seconds}>{seconds}</p> Sec</span>
    </Card>
  );
};
