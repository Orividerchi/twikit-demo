import React from "react";
import {
  List,
  ListItem,
  makeStyles,
  createStyles,
} from "@material-ui/core";

import { TimerCard } from "./TimerCard";
import { useAppSelector } from "../store/hooks";
import { selectTimers } from "../store/timers/selectors";
import { selectOrder } from "../store/ui/selectors";

export interface TimerListProps {}
const useStyles = makeStyles(() =>
  createStyles({
    list: {
      margin: "0 auto",
      width: 400,
      flexDirection: 'column',
      display: 'flex'
    },
    reverse: {
      flexDirection: 'column-reverse',
    }
  })
);
export const TimerList: React.FunctionComponent<TimerListProps> = () => {
  const classes = useStyles();
  const timers = useAppSelector(selectTimers);
  const isAscOrder = useAppSelector(selectOrder);
  return (
    <List className={`${classes.list} ${isAscOrder && classes.reverse}`}>
      {timers.map((timer) => (
        <ListItem key={timer.id}>
          <TimerCard timer={timer} />
        </ListItem>
      ))}
    </List>
  );
};
