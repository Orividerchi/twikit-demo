import React from "react";
import { Button, createStyles, makeStyles } from "@material-ui/core";

import { useAppDispatch } from "../store/hooks";
import { changeOrder } from "../store/ui/actions";
import { removeAll } from "../store/timers/actions";
import order from "../assets/order.svg";
import refresh from "../assets/refresh.svg";

const useStyles = makeStyles(() =>
  createStyles({
    icon: {
      width: 20,
      height: 20,
    },
    container: {
      margin: "0 auto",
      width: 400,
      flexDirection: "row",

    },
    button: {
      border: "2px solid #fad7cd",
    },
    buttonContainer: {
        justifyContent: "space-between",
        display: "flex",
        margin: '0 15px 0 15px'
    },
    name: {
        color: '#e64311'
    }
  })
);

interface HeaderProps {}

export const Header: React.FunctionComponent<HeaderProps> = () => {
  const classes = useStyles();

  const dispatch = useAppDispatch();
  const onChangeOrderClick = () => dispatch(changeOrder());
  const onRestartClick = () => dispatch(removeAll());

  return (
    <div className={classes.container}>
      <h1 className={classes.name}>Twikit Time-it</h1>

      <div className={classes.buttonContainer}>
        <Button className={classes.button} onClick={onChangeOrderClick}>
          Change order
          <img className={classes.icon} src={order} alt="" />
        </Button>
        <Button className={classes.button} onClick={onRestartClick}>
          RESTART
          <img className={classes.icon} src={refresh} alt="" />
        </Button>
      </div>
    </div>
  );
};
