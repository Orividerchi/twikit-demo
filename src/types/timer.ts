export type Timer = {
    id: string;
    status: string;
    seconds: number;
    createdDate: number;
}
