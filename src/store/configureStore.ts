import { configureStore } from "@reduxjs/toolkit";
import timerReducer, { TimersState } from "./timers/reducer";
import uiReducer, { UiState } from "./ui/reducer";

export const store = configureStore({
  reducer: {
    timers: timerReducer,
    ui: uiReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export interface RootState {
  timers: TimersState;
  ui: UiState;
}
