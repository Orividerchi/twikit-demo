import { Timer } from "../../types/timer";

export const ADD_TIMER = "ADD_TIMER";
export const REMOVE_ALL = "REMOVE_ALL";

export const addTimer = (timer: Timer) => ({
  type: ADD_TIMER,
  payload: timer,
});

export const removeAll = () => ({
  type: REMOVE_ALL,
});
