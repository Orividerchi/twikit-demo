import { v4 as uuid } from 'uuid';

import { Timer } from "../../types/timer";
import { TIMER_STATUSES } from "../../constants";
import { ADD_TIMER, REMOVE_ALL } from "./actions";
import { Action } from 'redux';

export interface TimersState {
  timers: Timer[];
}

const getDefaultTimer = (): Timer => ({
  id: uuid(),
  seconds: 0,
  status: TIMER_STATUSES.START,
  createdDate: Date.now(),
});

const initialState: TimersState = {
  timers: [getDefaultTimer()],
};

export default function timerReducer<TimersState>(
  state = initialState,
  action: Action
) {
  switch (action.type) {
    case ADD_TIMER: {
      const { timers } = state;
      return { timers: [...timers, getDefaultTimer()] };
    }
    case REMOVE_ALL: {
      return { timers: [getDefaultTimer()] };
    }
    default:
      return state;
  }
}
