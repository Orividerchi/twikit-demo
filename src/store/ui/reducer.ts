import { Action } from "redux";
import { CHANGE_ORDER } from "./actions";

export interface UiState {
  ascOrder: boolean;
}

const initialState: UiState = {
  ascOrder: true,
};

export default function uiReducer<UiState>(state = initialState, action: Action) {
  switch (action.type) {
    case CHANGE_ORDER: {
      const { ascOrder } = state;
      return { ascOrder: !ascOrder };
    }
    default:
      return state;
  }
}
